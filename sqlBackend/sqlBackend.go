package sqlBackend

import (
	"database/sql"
	"errors"
	"os"
)

// SqlAuthBackend database and database connection information.
type SqlAuthBackend struct {
	DriverName     string
	DataSourceName string
	Db             *sql.DB

	// prepared statements
	userStmt   *sql.Stmt
	usersStmt  *sql.Stmt
	insertStmt *sql.Stmt
	updateStmt *sql.Stmt
	deleteStmt *sql.Stmt
}


func mksqlerror(msg string) error {
	return errors.New("sqlbackend: " + msg)
}

var (
	ErrMissingBackend = errors.New("gobfilebackend: missing backend")
)

func NewSqlAuthBackend(DriverName, DataSourceName string) (b SqlAuthBackend, e error) {
	b.DriverName = DriverName
	b.DataSourceName = DataSourceName
	if DriverName == "sqlite3" {
		if _, err := os.Stat(DataSourceName); os.IsNotExist(err) {
			return b, ErrMissingBackend
		}
	}
	Db, err := sql.Open(DriverName, DataSourceName)
	if err != nil {
		return b, mksqlerror(err.Error())
	}
	err = Db.Ping()
	if err != nil {
		return b, mksqlerror(err.Error())
	}
	b.Db = Db
	_, err = Db.Exec(`
			  CREATE TABLE IF NOT EXISTS users (
			  id serial NOT NULL,
			  email character varying(254) NOT NULL,
			  phone character varying(32) NOT NULL,
			  username character varying(16) NOT NULL,
			  password character varying(64) NOT NULL,
			  confirm integer NOT NULL DEFAULT 0,
			  reg_type integer NOT NULL DEFAULT 1,
			  logins integer NOT NULL DEFAULT 0,
			  last_login integer,
			  country_id integer,
			  hash character varying(32) NOT NULL,
			  restore character varying(32) NOT NULL,
			  chk_code integer DEFAULT 0,
			  dummy smallint NOT NULL DEFAULT (0)::smallint,
			  pin character varying(32),
			  reg_date timestamp with time zone NOT NULL DEFAULT now(),
			  CONSTRAINT users_pkey PRIMARY KEY (id)
                    	)`)
	if err != nil {
		return b, mksqlerror(err.Error())
	}

	_, err = Db.Exec(`
			  	CREATE TABLE IF NOT EXISTS dummies (
			  	id serial NOT NULL,
  				user_id integer NOT NULL,
  				name character varying(32) NOT NULL,
  				password character varying(64) NOT NULL,
  				active smallint NOT NULL DEFAULT (0)::smallint,
  				devices character varying(2044),
  				CONSTRAINT dummies_key_id PRIMARY KEY (id),
  				CONSTRAINT dummies_name_key UNIQUE (name)
                    	)`)
	if err != nil {
		return b, mksqlerror(err.Error())
	}
	return b, nil

	_, err = Db.Exec(`
			  	CREATE TABLE IF NOT EXISTS devices (
			  	id serial NOT NULL,
  				user_id integer NOT NULL,
  				name character varying(50) NOT NULL,
  				key character varying(50) NOT NULL,
  				type character varying(10) NOT NULL,
  				CONSTRAINT devices_pkey PRIMARY KEY (id)
                    	)`)
	if err != nil {
		return b, mksqlerror(err.Error())
	}

	_, err = Db.Exec(`
			  CREATE TABLE IF NOT EXISTS playlists(
			  id serial NOT NULL,
			  user_id integer NOT NULL,
			  name character varying(32) NOT NULL,
			  title character varying(32) NOT NULL,
			  original boolean NOT NULL DEFAULT true,
			  source character varying(32),
			  type character varying(8) NOT NULL,
			  filename character varying(64) NOT NULL,
			  sort integer NOT NULL DEFAULT 0,
			  update boolean NOT NULL DEFAULT false,
			  update_url character varying(256),
			  update_period integer NOT NULL DEFAULT 0,
			  last_update timestamp without time zone,
			  archive boolean DEFAULT false,
			  is_aerospike boolean NOT NULL DEFAULT false,
			  last_touch timestamp without time zone,
			  backuped boolean DEFAULT false,
			  CONSTRAINT playlists_pkey PRIMARY KEY (id)
			)`)
	if err != nil {
		return b, mksqlerror(err.Error())
	}

	_, err = Db.Exec(`
			  	CREATE TABLE IF NOT EXISTS playlists_device(
			  	id serial NOT NULL,
  				user_id integer NOT NULL,
  				device_id integer NOT NULL,
  				playlist_id integer NOT NULL,
  				state boolean NOT NULL DEFAULT false,
  				CONSTRAINT playlists_device_pkey PRIMARY KEY (id)
			)`)
	if err != nil {
		return b, mksqlerror(err.Error())
	}

	_, err = Db.Exec(`CREATE TABLE IF NOT EXISTS devices_opt(
  			  id serial NOT NULL,
  			  device_id integer NOT NULL,
  			  val character varying(2048) NOT NULL,
  			  key character varying(20) NOT NULL,
  			  CONSTRAINT devices_opt_pkey PRIMARY KEY (id)
			)`)
	if err != nil {
		return b, mksqlerror(err.Error())
	}


	_, err = Db.Exec(`CREATE TABLE IF NOT EXISTS groups(
  			  id serial NOT NULL,
  			  user_id integer NOT NULL,
  			  playlist_id integer NOT NULL,
  			  name character varying(32) NOT NULL,
  			  title character varying(32) NOT NULL,
  			  hide boolean NOT NULL DEFAULT false,
  			  "order" integer NOT NULL DEFAULT 0,
  			  CONSTRAINT groups_pkey PRIMARY KEY (id)
			)`)
	if err != nil {
		return b, mksqlerror(err.Error())
	}

	_, err = Db.Exec(`CREATE TABLE events	(
  			  id serial NOT NULL,
  			  channel_id integer,
  			  start timestamp without time zone,
  			  stop timestamp without time zone,
  			  title character varying(1024),
  			  "desc" character varying(4096),
  			  CONSTRAINT evnt_id_pk PRIMARY KEY (id)
			)`)
	if err != nil {
		return b, mksqlerror(err.Error())
	}

	return b, nil
}
