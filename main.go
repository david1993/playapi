package main

import (
	"jsonrpc"
	"github.com/gin-gonic/gin"
	"strings"
	"fmt"
	"sqlBackend"
	_"github.com/lib/pq"
	"math/rand"
    "time"
	"crypto/md5"
    "encoding/hex"
	"crypto/hmac"
	"crypto/sha256"
	"database/sql"
	"strconv"
	"net/http"
	"net/url"
	"session"
	"encoding/json"
	"crypto/sha1"
	. "github.com/aerospike/aerospike-client-go"
	"github.com/bradfitz/slice" 
)

const (  
  host     = "localhost"
  port     = 5432
  user     = "postgres"
  password = "gjmgjm"
  dbname   = "playapi"
)

type User struct {
    Skey string
    LiveTime int
	sqlBackend sqlBackend.SqlAuthBackend
	Sessions []session.Session
}

func NewUser(hash_key string, live_time int) User {
	user := User{}
	user.Skey = hash_key
	user.LiveTime = live_time
	return user
}

func (s *User) Connect_to_db() {
	var err error
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+ "password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	s.sqlBackend, err = sqlBackend.NewSqlAuthBackend("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
}

func (s *User) GenChkcode() int {
	rand.Seed(time.Now().Unix())
    return rand.Intn(10000)	
}

func (s *User) SendSms(telephone string, chk_code int) bool {
        smsapiurl := "http://alphasms.com.ua/api/http.php"
        key := "84aa2665fbb5d6cc16ff35a1bf0376a688b9bef5"
        from := "ottplayer"
        to := telephone;
        text := strconv.Itoa(chk_code)
        resp, _ := http.PostForm(smsapiurl, url.Values{"kversion": {"http"}, "key": {key}, "command": {"send"}, "from": {from}, "to": {to}, "message": {text}})
		fmt.Printf("%s", resp)
        return true
    }

func (s *User) GetMD5Hash(text string) string {
    hasher := md5.New()
    hasher.Write([]byte(text))
    return hex.EncodeToString(hasher.Sum(nil))
}

func (s *User) ComputeHmac256(message string, secret string) string {
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(message))
	return hex.EncodeToString(h.Sum(nil))
}

func (s *User) IsValidNumber(number string) bool {
	if (len(number) < 7) {
		return false
	}
	AllowedChars:= "0123456789"
	for _, element := range number {
		c := string(element)
		i := strings.Index(AllowedChars, c)
		if (i == -1) {
			return false
		}
	}
	return true
}

func (s *User) RegisterBySms(username string, telephone string, pass string) (string, string) {
	if (!s.IsValidNumber(telephone)) {
		return "null","Telephone format invalid"
	}

	if (len(username) < 3) {
		return "null","Username length is small"
	}

	userIdStmt, err := s.sqlBackend.Db.Prepare(`select id from users where phone=$1 and reg_type=2 limit 1`)
	if err != nil {
		return "null", fmt.Sprintf("userIdStmt: %v", err)
	}
	row := userIdStmt.QueryRow(telephone)
	var id int
	err = row.Scan(&id)
	if err != sql.ErrNoRows {
		return "null", "Already exist telephone"
	}

	insrtUsrStmt, err := s.sqlBackend.Db.Prepare(`insert into users (email, phone, username, password, reg_type, hash, restore, chk_code) VALUES ($1 || '@ottplayer.es',$2,$3,$4,2,$5,$6,$7)`)
	if err != nil {
		return "null", fmt.Sprintf("userIdStmt: %v", err)
	}

	chkcode := s.GenChkcode()

	_, err = insrtUsrStmt.Exec(telephone, telephone, username, s.ComputeHmac256(pass, s.Skey), s.GetMD5Hash(time.Now().String() + telephone), s.GetMD5Hash(username + s.GetMD5Hash(telephone)), chkcode)

	if (s.SendSms(telephone, chkcode)) {
		return "ok", "null"
	} else {
		return "null", "Error send sms"
	}
}

func (s *User) Login(login string, pass string, dev_key string) (string, string) {
	i := strings.Index(login, "@")
	if (i == -1) {
		dummiesStmt, err := s.sqlBackend.Db.Prepare(`select id, password, devices from dummies where name=$1 and active=1 limit 1`)
		if err != nil {
			return "null", fmt.Sprintf("dummiesStmt: %v", err)
		}
		row := dummiesStmt.QueryRow(login)
		var id int
		var password string
		var devices sql.NullString
		err = row.Scan(&id, &password, &devices)
		if err != sql.ErrNoRows {
			if s.ComputeHmac256(pass, s.Skey) == password {
				userIdStmt, err := s.sqlBackend.Db.Prepare(`select email from users where id=$1 limit 1`)
				if err != nil {
					return "null", fmt.Sprintf("userIdStmt: %v", err)
				}
				row := userIdStmt.QueryRow(login)
				var email string
				err = row.Scan(&email)
				if err != sql.ErrNoRows {
					if devices.Valid {
						sess := s.AddUserSession(id, email, dev_key, id, devices.String)
						return sess.Hash, "null"
					}
				} else {
					return "null", "Fake parrent login not find"
				}
			} else {
				return "null", "Fake password wrong"
			}
		} else {
			return "null", "Fake login not found"
		}
	} else {
		userIdStmt, err := s.sqlBackend.Db.Prepare(`select id, password from users where email=$1 limit 1`)
		if err != nil {
			return "null", fmt.Sprintf("userIdStmt: %v", err)
		}
		row := userIdStmt.QueryRow(login)
		var id int
		var password string
		err = row.Scan(&id, &password)
		if err != sql.ErrNoRows {
			if s.ComputeHmac256(pass, s.Skey) == password {
				sess := s.AddUserSession(id, login, dev_key, 0, "")
				return sess.Hash, "null"
			} else {
				return "null", "Password wrong"
			}
		} else {
			return "null", "Login not found"
		}
	}
	return "null", "null"
}

func (s *User) LoginPhone(phone string, pass string, dev_key string) (string, string) {
	userIdStmt, err := s.sqlBackend.Db.Prepare(`select id, password from users where phone=$1 limit 1`)
	if err != nil {
		return "null", fmt.Sprintf("userIdStmt: %v", err)
	}
	row := userIdStmt.QueryRow(phone)
	var id int
	var password string
	err = row.Scan(&id, &password)
	if err != sql.ErrNoRows {
		if s.ComputeHmac256(pass, s.Skey) == password {
			sess := s.AddUserSession(id, phone, dev_key, 0, "")
			return sess.Hash, "null"
		} else {
			return "null", "Password wrong"
		}
	} else {
		return "null", "Login not found"
	}
	return "null", "null"
}

func (s *User) Logout(hash string) (string, string) {
	for _, element := range s.Sessions {
		if hash == element.Hash {
			element.IsLogout = true
		}
	}
	return "done", "null"
}

func (s *User) ChkDevice(hash string, dev_key string) (string, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		if s.FindDevice(&sess, dev_key) {
			return "Ok", "null"
		} else {
			return "null", "Device not registred"
		}
	}
	return "null", "null"
}

func (s *User) RegisterDevice(hash string, name string, tp string) (string, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		h := sha1.New()
		t := time.Now()
		h.Write([]byte(name + tp + t.String()))
    	hkey := hex.EncodeToString(h.Sum(nil))
		insrtDevStmt, err := s.sqlBackend.Db.Prepare(`insert into devices (user_id, name, key, type) VALUES ($1,$2,$3,$4)`)
		if err != nil {
			return "null", fmt.Sprintf("insrtDevStmt: %v", err)
		}
		res, err := insrtDevStmt.Exec(sess.Id, name, hkey, tp)
		affected,_ := res.RowsAffected()
		
		if affected == 1 {
			if sess.FakeId > 0 {
				selectDevStmt, err := s.sqlBackend.Db.Prepare(`select id from users where user_id=$1 and key=$2 limit 1`)
				if err != nil {
					return "null", fmt.Sprintf("selectDevStmt: %v", err)
				}
				row := selectDevStmt.QueryRow(sess.Id, hkey)
				var id int
				err = row.Scan(&id)
				if err != sql.ErrNoRows {
					sess.Add_fake_dev(id)
					dlist := sess.Get_fake_devs_list()
					updDevStmt, err := s.sqlBackend.Db.Prepare(`update dummies set devices=$1 where id=$2`)
					if err != nil {
						return "null", fmt.Sprintf("updDevStmt: %v", err)
					}
					res, err = updDevStmt.Exec(dlist, sess.FakeId)
					affected, _ = res.RowsAffected()
					if affected < 1 {
						hkey = ""
					}
				}
			}
		} else {
			hkey = ""
		}
		if s.FindDevice(&sess, hkey) {
			return hkey, "null"
		} else {
			return "null", "Device register error" 
		}
	}
}

func (s *User) GetDevopt(key string, hash string) (string, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		selectValStmt, err := s.sqlBackend.Db.Prepare(`select val from devices_opt where device_id=$1 and key=$2 limit 1`)
		if err != nil {
			return "null", fmt.Sprintf("selectValStmt: %v", err)
		}
		row := selectValStmt.QueryRow(sess.DevId, key)
		var val string
		err = row.Scan(&val)
		if err != sql.ErrNoRows {
			return val, "null" 
		} else {
			return "null", "Option not found"
		}
	}
}

func (s *User) Getopt(sess session.Session, key string, val *string) bool{
	selectValStmt, err := s.sqlBackend.Db.Prepare(`select val from devices_opt where device_id=$1 and key=$2 limit 1`)
	if err != nil {
		return false
	}
	row := selectValStmt.QueryRow(sess.DevId, key)
	err = row.Scan(&val)
	if err != sql.ErrNoRows {
		return true
	} else {
		return false
	}
}

func (s *User) SetDevopt(key string, val string, hash string) (string, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		if val == "" {
			return "null", "Error store option"
		} else {
			var tval string
			if s.Getopt(sess, key, &tval) {
				updOptStmt, err := s.sqlBackend.Db.Prepare(`update devices_opt set val=$1 where device_id=$2 and key=$3`)
				if err != nil {
					return "null", fmt.Sprintf("updOptStmt: %v", err)
				}
				res, err := updOptStmt.Exec(val, sess.DevId, key)
				affected, _ := res.RowsAffected()
				if affected == 1 {
					return "Ok", "null"
				} else {
					return "null", "Error store option"
				}
			} else {
				insrtOptStmt, err := s.sqlBackend.Db.Prepare(`insert into devices_opt (device_id, val, key) VALUES ($1,$2,$3)`)
				if err != nil {
					return "null", fmt.Sprintf("insrtOptStmt: %v", err)
				}
				res, err := insrtOptStmt.Exec(sess.DevId, val, key)
				affected, _ := res.RowsAffected()
				if affected == 1 {
					return "Ok", "null"
				} else {
					return "null", "Error store option"	
				}
			}
		}
	}
}

func (s *User) GetPlaylists(hash string) (interface{}, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		rows, err := s.sqlBackend.Db.Query("select pl.id, pl.name, pl.title, pl.archive from playlists pl, playlists_device d where pl.user_id=$1 and d.user_id = pl.user_id and pl.id=d.playlist_id and d.device_id=$2 and d.state=true", sess.Id, sess.DevId)  
		if err != nil {
        	return "null", fmt.Sprintf("playlists: %v", err)
  		}

		type Obj struct {
    		Id int
    		Name string
    		Title string
			Archive bool
		}

		var arr []Obj
		
		for rows.Next() {
    		var id int
    		var name, title string
			var archive bool
    		rows.Scan(&id, &name, &title, &archive)

			m := Obj{id, name, title, archive}
			arr = append(arr, m)	
  		}
	
		jsonArr, err := json.Marshal(arr)
		if err != nil {
			return "null", fmt.Sprintf("Cannot encode to JSON : %v", err)
		}
		return jsonArr, "null"
	}
}

func (s *User) GetGroups(hash string) (interface{}, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		rows, err := s.sqlBackend.Db.Query("select id, name, title from groups where user_id=$1 and hide = false and order by order", sess.Id)  
		if err != nil {
        	return "null", fmt.Sprintf("groups: %v", err)
  		}

		type Obj struct {
    		Id int
    		Name string
    		Title string
		}

		var arr []Obj
		
		for rows.Next() {
    		var id int
    		var name, title string
    		rows.Scan(&id, &name, &title)

			m := Obj{id, name, title}
			arr = append(arr, m)	
  		}
	
		jsonArr, err := json.Marshal(arr)
		if err != nil {
			return "null", fmt.Sprintf("Cannot encode to JSON : %v", err)
		}
		return jsonArr, "null"
	}
}

func (s *User) FindEpg(name string, hash string) (interface{}, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		rows, err := s.sqlBackend.Db.Query("select id, lev($1,name) as weight, name from channels where lev($2,name) > 0.6 order by weight desc", name, name)  
		if err != nil {
        	return "null", fmt.Sprintf("epgs: %v", err)
  		}

		type Obj struct {
    		Id int
    		Weight int
    		Name string
		}

		var arr []Obj
		
		for rows.Next() {
    		var id, weight int
    		var name string
    		rows.Scan(&id, &weight, &name)

			m := Obj{id, weight, name}
			arr = append(arr, m)	
  		}
	
		jsonArr, err := json.Marshal(arr)
		if err != nil {
			return "null", fmt.Sprintf("Cannot encode to JSON : %v", err)
		}
		return jsonArr, "null"
	}
}

func (s *User) FindEpg2(name string) (interface{}, string) {
	rows, err := s.sqlBackend.Db.Query("select id, lev($1,name) as weight, name from channels where lev($2,name) > 0.6 order by weight desc", name, name)  
	if err != nil {
	return "null", fmt.Sprintf("epgs: %v", err)
	}

	type Obj struct {
	Id int
	Weight int
	Name string
	}

	var arr []Obj
	
	for rows.Next() {
	var id, weight int
	var name string
	rows.Scan(&id, &weight, &name)

		m := Obj{id, weight, name}
		arr = append(arr, m)	
	}

	jsonArr, err := json.Marshal(arr)
	if err != nil {
		return "null", fmt.Sprintf("Cannot encode to JSON : %v", err)
	}
	return jsonArr, "null"
}

func (s *User) GetEpg(id int, period int, hash string) (interface{}, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		rows, err := s.sqlBackend.Db.Query("select id, start, stop, title, desc from events where channel_id=$1 and (start>current_date - interval '4 day')", id)  
		if err != nil {
        	return "null", fmt.Sprintf("events: %v", err)
  		}

		type Obj struct {
    		Id int
    		Start string
    		Stop string
		Title string
		Desc string
		}

		var arr []Obj
		
		for rows.Next() {
    		var id int
    		var start, stop, title, desc string
    		rows.Scan(&id, &start, &stop, &title, &desc)

			m := Obj{id, start, stop, title, desc}
			arr = append(arr, m)	
  		}
	
		jsonArr, err := json.Marshal(arr)
		if err != nil {
			return "null", fmt.Sprintf("Cannot encode to JSON : %v", err)
		}
		return jsonArr, "null"
	}
}

func (s *User) GetEpg2(id int, period int, back_days int) (interface{}, string) {
	type Obj struct {
	Id int
	Start string
	Stop string
	Title string
	Desc string
	}

	var arr []Obj

	if period < 1 || back_days < 0 {
		jsonArr, _ := json.Marshal(arr)
		return jsonArr, "null"
	}
	
	rows, err := s.sqlBackend.Db.Query("select id, start, stop, title, desc from events where channel_id=$1 and (start>current_date - interval '$2 day')", id, back_days)  
	if err != nil {
	return "null", fmt.Sprintf("events: %v", err)
	}
	
	for rows.Next() {
	var id int
	var start, stop, title, desc string
	rows.Scan(&id, &start, &stop, &title, &desc)

		m := Obj{id, start, stop, title, desc}
		arr = append(arr, m)	
	}

	jsonArr, err := json.Marshal(arr)
	if err != nil {
		return "null", fmt.Sprintf("Cannot encode to JSON : %v", err)
	}
	return jsonArr, "null"
}

func (s *User) GetEpgGroup(ids string, period int, back_days int) (interface{}, string) {
	type Obj struct {
	Id int
	Start string
	Stop string
	Title string
	Desc string
	}

	var arr []Obj

	if period < 1 || back_days < 0 {
		jsonArr, _ := json.Marshal(arr)
		return jsonArr, "null"
	}
	
	rows, err := s.sqlBackend.Db.Query("select id, start, stop, title, desc from events where channel_id in (" + ids + ") and (start>current_date - interval '$1 day')", back_days)  
	if err != nil {
		return "null", fmt.Sprintf("events: %v", err)
	}
	
	for rows.Next() {
	var id int
	var start, stop, title, desc string
	rows.Scan(&id, &start, &stop, &title, &desc)

		m := Obj{id, start, stop, title, desc}
		arr = append(arr, m)	
	}

	jsonArr, err := json.Marshal(arr)
	if err != nil {
		return "null", fmt.Sprintf("Cannot encode to JSON : %v", err)
	}
	return jsonArr, "null"
}

func (s *User) GetChannels(pl_id int, hash string) (interface{}, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		client, err := NewClient("127.0.0.1", 3000)
	  	if err != nil {
			panic(err)
	  	}	

		type ChannelRecord struct{
			Id string
			Plid int  
			Gid int
			Title string
			LibchId int
			LibchLike int
			Adult bool
			Href string
			Type string
			Order int
			EpgId int
			Icon int
		} 

		type Obj struct{
			Id int
			Name string  
			Title string
			GroupId int
			Href string
			Type string
			Pict string
			Adult bool
			Order int
			EpgId int
			LibId int
			Icon int
		} 

		var ChannelRecords []ChannelRecord
		var arr []Obj

		stm := NewStatement("ottplayer", "channels")
	
		stm.Addfilter(NewEqualFilter("plid", pl_id))
		recordset, err := client.Query(nil, stm)
		if err != nil {
		    panic(err)
		}


		for result := range recordset.Results() {
		    if result.Err != nil {
		        panic(err)
		    }
		    fmt.Println("found record %v", result.Record)

			asid := result.Record.Bins["id"].(string)
			plid := result.Record.Bins["plid"].(int)
			gid := result.Record.Bins["gid"].(int)
			astitle := result.Record.Bins["title"].(string)
			ashref := result.Record.Bins["href"].(string)
			astype := result.Record.Bins["type"].(string)
			adult := result.Record.Bins["adult"].(bool)
			epg_id := result.Record.Bins["epg_id"].(int)
			order := result.Record.Bins["order"].(int)
			icon := result.Record.Bins["icon"].(int)
			libch_id := result.Record.Bins["libch_id"].(int)

		
		    crec := ChannelRecord{asid, plid, gid, astitle, libch_id,  0,  adult, ashref, astype, order, epg_id, icon}
			ChannelRecords = append(ChannelRecords, crec)
		}

		slice.Sort(ChannelRecords, func(i, j int) bool {
	  		return ChannelRecords[i].Order < ChannelRecords[j].Order
		})

		for index,element := range ChannelRecords {
			id := index
			name := element.Title
			title := element.Title
			grp_id := element.Gid
			href := element.Href
			tpe := element.Type
			pic := ""
			order := element.Order
			var adult bool
			adult = element.Adult
			epg_id := element.EpgId
			libid := element.LibchId
			icon := element.Icon

			if icon > 0 {
				pic = "http://widget.ottplayer.es/upictures/"+element.Id+".png"
			} else {
				if libid > 0 {
					pic = "http://widget.ottplayer.es/pictures/"+strconv.Itoa(libid)+".png";
				}
			}
		
			var epg int		

			if libid > 0 {

				rows, err := s.sqlBackend.Db.Query("select epg from libchannels where id=$1", libid)  
				if err != nil {
					return "null", fmt.Sprintf("epgs: %v", err)
				}
	
				for rows.Next() {
					rows.Scan(&epg)
				}
				m := Obj{id, name, title, grp_id, href, tpe, pic, adult, order, epg_id, libid, icon}
				arr = append(arr, m)
			}
		}
		return arr, "null"
	}
}

func (s *User) GetDevices(filter string, hash string) (interface{}, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		rows, err := s.sqlBackend.Db.Query("select id, name, key from devices where user_id=$1 and type=$2", sess.Id, filter)  
		if err != nil {
        	return "null", fmt.Sprintf("groups: %v", err)
  		}

		type Obj struct {
    		Name string
    		Key string
		}

		var arr []Obj
		
		for rows.Next() {
    		var id int
    		var name, key string
    		rows.Scan(&id, &name, &key)
			if sess.FakeId > 0 {
				if !sess.Check_fake_access(id) {
					continue
				}
			}
			m := Obj{name, key}
			arr = append(arr, m)	
  		}
	
		jsonArr, err := json.Marshal(arr)
		if err != nil {
			return "null", fmt.Sprintf("Cannot encode to JSON : %v", err)
		}
		return jsonArr, "null"
	}
}

func (s *User) DeletePlaylist(pl_id int, hash string) (string, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		if pl_id < 1 {
			return "null", "Error delete playlist"
		} 
		if sess.FakeId > 0 {
			return "null", "Error delete playlist"
		}
		delDevStmt, err := s.sqlBackend.Db.Prepare(`delete from playlists where id=$1 and user_id=$2`)
		if err != nil {
			return "null", fmt.Sprintf("delDevStmt: %v", err)
		}
		res, err := delDevStmt.Exec(pl_id, sess.Id)
		affected, _ := res.RowsAffected()
		if affected != 1 {
			return "null", "Error delete playlist"
		}

		delDevStmt1, err := s.sqlBackend.Db.Prepare(`delete from playlists_device where playlist_id=$1 and user_id=$2`)
		if err != nil {
			return "null", fmt.Sprintf("delDevStmt1: %v", err)
		}
		res, err = delDevStmt1.Exec(pl_id, sess.Id)

		delDevStmt2, err := s.sqlBackend.Db.Prepare(`delete from groups where playlist_id=$1 and user_id=$2`)
		if err != nil {
			return "null", fmt.Sprintf("delDevStmt2: %v", err)
		}
		res, err = delDevStmt2.Exec(pl_id, sess.Id)

		return "OK", "null"
	}
}

func (s *User) VerifyCode(telephone string, chkcode int) (string, string) {
	userIdStmt, err := s.sqlBackend.Db.Prepare(`select id from users where phone=$1 and chk_code=$2 and confirm=0 limit 1`)
	if err != nil {
		return "null", fmt.Sprintf("userIdStmt: %v", err)
	}
	row := userIdStmt.QueryRow(telephone, chkcode)
	var id int
	err = row.Scan(&id)
	if err != sql.ErrNoRows {
		updUsrStmt, err := s.sqlBackend.Db.Prepare(`update users set confirm=1 where id=$1`)
		if err != nil {
			return "null", fmt.Sprintf("updUsrStmt: %v", err)
		}
		res, err := updUsrStmt.Exec(id)
		affected, _ := res.RowsAffected()
		if affected == 1 {
			return "null", "OK"
		}
	}
	return "Invalid check code", "null"
}

func (s *User) GetTime() (string, string) {
	t := time.Now()
	return t.String(), "null"
}

func (s *User) GetUploadhash(hash string) (string, string) {
	sess := s.LoginByHash(hash)
	if sess.Is_expired() || sess.Logout() {
		return "null", "Session expired"
	} else {
		userHashStmt, err := s.sqlBackend.Db.Prepare(`select hash from users where id=$1 limit 1`)
		if err != nil {
			return "null", fmt.Sprintf("userHashStmt: %v", err)
		}
		row := userHashStmt.QueryRow(sess.Id)
		var hash string
		err = row.Scan(&hash)
		if err != sql.ErrNoRows {
			return hash, "null"
		} else {
			return "null", "There is no user"
		}
	}
}

func (s *User) AddUserSession(id int , login string, dev_key string, fake int, devices string) session.Session {
	sess := session.NewSession(id, login, s.LiveTime, fake, devices)
	if dev_key != "" {
		s.FindDevice(&sess, dev_key)
	}
	s.Sessions = append(s.Sessions, sess)
	return sess
}

func (s *User) FindDevice(sess *session.Session, dev_key string) bool {
		devicesStmt, err := s.sqlBackend.Db.Prepare(`select id from devices where user_id=$1 and key=$2 imit 1`)
		if err != nil {
			return false
		}
		row := devicesStmt.QueryRow(sess.Id, dev_key)
		var id int
		err = row.Scan(&id)
		if err != sql.ErrNoRows {
			if sess.FakeId > 0 {
				if !sess.Check_fake_access(id) {
					return false
				}
			}
			sess.DevId = id
			return true
		}
		return false
}

func (s *User) LoginByHash(hash string) session.Session {
	for _, element := range s.Sessions {
		if hash == element.Hash {
			return element
		}
	}
	return session.Session{}
}



func main() {
	usr := NewUser("Hash key", 100)
	usr.Connect_to_db()

	router := gin.Default()
	router.POST("/", func(c *gin.Context) { jsonrpc.ProcessJsonRPC(c, &usr); })
	router.Run("127.0.0.1:8001")
}
