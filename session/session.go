package session

import (
	"crypto/sha1"
	"time"
	"strings"
	"strconv"
	"encoding/hex"
)


type Session struct {
    Id int
    Login string
    Hash string
    LiveTime int
    DevId int 
    IsLogout bool
    FakeId int
    FakeDevices []int
    Stime time.Time
}

func NewSession(Id int, Login string, live_time int, fake int, devices string) Session {
	session := Session{}
	session.DevId = -1
	session.IsLogout = false
	session.Id = Id	
	session.Login = Login
	session.LiveTime = live_time
	session.FakeId = fake
	if fake > 0 {
		session.AddFakeDevices(devices)
	}

	h := sha1.New()
	t := time.Now()
	h.Write([]byte(Login + t.String()))
    	session.Hash = hex.EncodeToString(h.Sum(nil))
	return session
}

func (s *Session) Is_expired() bool {
	t := time.Now()
	diff := t.Sub(s.Stime)
    if (int(diff.Minutes()) > s.LiveTime) {
	return true
    }
    return false
}

func (s *Session) Logout() bool {
	return s.IsLogout
}

func (s *Session) Update() {
	s.Stime = time.Now()
}

func (s *Session) RenewHash() {
	h := sha1.New()
	t := time.Now()
	h.Write([]byte(s.Login + t.String()))
    	s.Hash = hex.EncodeToString(h.Sum(nil))
	s.Update()
}

func (s *Session) AddFakeDevices(devices string) {
	s.FakeDevices = make([]int, 0) 
    devs := strings.Split(devices, ",")
	for _, element := range devs {
	it, _ := strconv.Atoi(element)
		s.FakeDevices = append(s.FakeDevices, it)
	}
}

func (s *Session) Check_fake_access(deviceId int) bool {
	for _, element := range s.FakeDevices {
		if (element == deviceId) {
			return true
		}
	}
	return false
}

func (s *Session) Add_fake_dev(deviceId int) {
	s.FakeDevices = append(s.FakeDevices, deviceId)
}


func (s *Session) Get_fake_devs_list() string {
	var buff string
	for index, element := range s.FakeDevices {
		buff += strconv.Itoa(element)
		if (index != len(s.FakeDevices) - 1) {
			buff += ","
		}
	}
	return buff
}






